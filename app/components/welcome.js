import './welcome.css';
import template from './welcome.tpl.html';

export default (app) => {
  app.component('welcome', {
    template
  });
};
