import './happyThoughtsGrid.css';
import template from './happyThoughtsGrid.tpl.html';

class HappyThoughtsGridController {
}

export default (app) => {
  app.component('happyThoughtsGrid', {
    template,
    controller: HappyThoughtsGridController,
    transclude: true,
    bindings: {
      thoughts: '<',
    }
  });
};
