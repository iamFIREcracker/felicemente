import './happyThoughtCard.css';
import template from './happyThoughtCard.tpl.html';

class HappyThoughtCardController {
}

export default (app) => {
  app.component('happyThoughtCard', {
    template,
    controller: HappyThoughtCardController,
    bindings: {
      card: '<',
      key: '<',
      onImageClick: '&',
      onImageError: '&',
      onNameChange: '&',
      onDateChange: '&',
      onDescriptionChange: '&',
      onDeleteButtonClick: '&'
    }
  });
};
