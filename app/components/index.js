import editableField from './editableField';
import happyThoughtCard from './happyThoughtCard';
import happyThoughtEntry from './happyThoughtEntry';
import happyThoughtsGrid from './happyThoughtsGrid';
import navbar from './navBar';
import welcome from './welcome';

export default (app) => {
  editableField(app);
  happyThoughtCard(app);
  happyThoughtEntry(app);
  happyThoughtsGrid(app);
  navbar(app);
  welcome(app);
};
