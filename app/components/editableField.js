import template from './editableField.tpl.html';

class EditableFieldController {
  constructor($scope) {
    this.$scope = $scope;
    this.init();
  }

  $onChanges(changes) {
    if (changes.fieldType || changes.value) {
      this.init();
    }
  }

  init() {
    if (this.fieldType === 'date' && this.value) {
      this.value = new Date(this.value);
    }
    this.originalValue = this.value;
  }

  onContainerClick() {
    this.editing = true;
  }

  onInputBlur() {
    this.value = this.originalValue;
    this.$scope.form.$setPristine();
    this.editing = false;
  }

  onFormSubmit() {
    this.onEdit({ value: this.value });
    // XXX hack to update the element without waiting for the parent
    // to trigger another update
    let value = this.value;
    if (this.fieldType === 'date') {
      value = new Date(value);
    }
    this.originalValue = value;
    this.editing = false;
  }

  static $inject = [
    '$scope',
  ]
}

export default (app) => {
  app.component('editableField', {
    template,
    controller: EditableFieldController,
    bindings: {
      fieldType: '@',
      placeholder: '@',
      value: '<',
      onEdit: '&'
    }
  });
};
