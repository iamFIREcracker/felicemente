import './navBar.css';
import template from './navBar.tpl.html';

class NavBarController {
}

export default (app) => {
  app.component('navBar', {
    template,
    controller: NavBarController,
    bindings: {
      loading: '<',
      googleEmail: '<',
      fbEmail: '<',
      onSignInButtonClick: '&',
      onSignOutButtonClick: '&',
      onFbSignInButtonClick: '&',
      onFbSignOutButtonClick: '&',
    }
  });
};
