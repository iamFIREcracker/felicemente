import './happyThoughtEntry.css';
import template from './happyThoughtEntry.tpl.html';

class HappyThoughtEntryController {
}

export default (app) => {
  app.component('happyThoughtEntry', {
    template,
    controller: HappyThoughtEntryController,
    bindings: {
      url: '<',
      src: '<',
      error: '<',
      loading: '<',
      onInputChange: '&',
      onInputSubmit: '&'
    }
  });
};
