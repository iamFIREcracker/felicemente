import angular from 'angular';
import ngRedux from 'ng-redux';

import configureStore from './store/configureStore';
import middlewares from './middleware';
import enhancers from './enhancers';

import components from './components';
import appContainer from './containers/app';
import directives from './directives';
import services from './services';

const app = angular.module('Felicemente', [
  'bootstrapLightbox',
  'rx',
  'ui.bootstrap',
  ngRedux,
]);

app.config([
  'LightboxProvider',
  (LightboxProvider) => {
    LightboxProvider.getImageUrl = image => image.src; // eslint-disable-line no-param-reassign

    LightboxProvider.getImageCaption = image => ( // eslint-disable-line no-param-reassign
      image.name
    );
  }
]);

app.config([
  '$ngReduxProvider',
  ($ngReduxProvider) => {
    configureStore($ngReduxProvider);
  }
]);

middlewares(app);
enhancers(app);
appContainer(app);
components(app);
directives(app);
services(app);
