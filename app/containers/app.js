import template from './app.tpl.html';
import {
  getEmail as getGoogleEmail,
  getLoadingStatus as getGoogleLoadingStatus
} from '../reducers/google';
import {
  getEmail as getFbEmail,
  getLoadingStatus as getFbLoadingStatus
} from '../reducers/facebook';
import {
  getEnteredUrl,
  getResolvedUrl,
  getEntryErrorStatus,
  getEntryLoadingStatus,
  getThoughts,
  getExampleThoughts
} from '../reducers/thoughts';
import * as actions from '../actions';
import * as googleActions from '../actions/google';
import * as fbActions from '../actions/facebook';

class AppController {
  constructor(
    $scope,
    $ngRedux,
    $window
  ) {
    this.$window = $window;
    const unsubscribe = $ngRedux.connect(
      AppController.mapStateToThis,
      this.mapDispatchToThis.bind(this) // XXX nice hack, baby!
    )(this);
    $scope.$on('$destroy', unsubscribe);
  }

  static mapStateToThis(state) {
    return {
      googleLoading: getGoogleLoadingStatus(state),
      googleEmail: getGoogleEmail(state),
      fbLoading: getFbLoadingStatus(state),
      fbEmail: getFbEmail(state),
      url: getEnteredUrl(state),
      src: getResolvedUrl(state),
      error: getEntryErrorStatus(state),
      entryLoading: getEntryLoadingStatus(state),
      thoughts: getThoughts(state),
      exampleThoughts: getExampleThoughts(state)
    };
  }

  mapDispatchToThis(dispatch) {
    return {
      onSignInButtonClick: () => dispatch(googleActions.logIn()),
      onSignOutButtonClick: () => dispatch(googleActions.logOut()),
      onFbSignInButtonClick: () => dispatch(fbActions.logIn()),
      onFbSignOutButtonClick: () => dispatch(fbActions.logOut()),
      onInputChange: url => dispatch(actions.extractImage(url)),
      onInputSubmit: (src, originalUrl) => dispatch(actions.addHappyThought({
        src,
        originalUrl,
        date: new Date()
      })),
      onImageError: (id, originalUrl) => dispatch(
        actions.reloadHappyThoughtImage(id, originalUrl)
      ),
      onImageClick: id => dispatch(actions.showHappyThought(id)),
      onNameChange: (id, name) => dispatch(
        actions.updateHappyThought(id, 'name', name)
      ),
      onDateChange: (id, date) => dispatch(
        actions.updateHappyThought(id, 'date', date)
      ),
      onDescriptionChange: (id, description) => dispatch(
        actions.updateHappyThought(id, 'description', description)
      ),
      onDeleteButtonClick: id => this.$window.confirm('Are you sure you want to delete it?')
        && dispatch(actions.deleteHappyThought(id)),
      showHappyThought: id => dispatch(actions.showHappyThought(id))
    };
  }

  static $inject = [
    '$scope',
    '$ngRedux',
    '$window'
  ]
}

export default (app) => {
  app.component('app', {
    template,
    controller: AppController
  });
};
