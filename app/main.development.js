import {
  app,
  ipcMain,
  screen,
  BrowserWindow,
  Menu,
  Tray,
} from 'electron';
import {
  version
} from './package.json';

let mainWindow = null;
let authWindow = null;
let appIcon = null;

if (process.env.NODE_ENV === 'production') {
  const sourceMapSupport = require('source-map-support'); // eslint-disable-line
  sourceMapSupport.install();
}

if (process.env.NODE_ENV === 'development') {
  require('electron-debug')({ showDevTools: true }); // eslint-disable-line global-require
}

require('electron-context-menu')(); // eslint-disable-line global-require

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit();
});

app.on('ready', () => {
  mainWindow = new BrowserWindow({
    title: 'Felicemente',
    width: (function widthAccordinglyToWorkspaceSize() {
      const workspaceWidth = screen.getPrimaryDisplay().workAreaSize.width;
      const wideEnough = (workspaceWidth * 7) / 8 >= 1200;
      let width = 800;
      if (wideEnough) {
        width = 1200;
      }
      return width;
    }()),
    height: (function heightAccordinglyToWorkspaceSize() {
      const workspaceHeight = screen.getPrimaryDisplay().workAreaSize.height;
      return parseInt((workspaceHeight * 8) / 9, 10);
    }()),
    center: true,
    show: false,
    autoHideMenuBar: true,
  });

  mainWindow.loadURL(`file://${__dirname}/app.html`);

  mainWindow.webContents.once('did-finish-load', () => {
    mainWindow.show();
    mainWindow.focus();
  });

  mainWindow.on('closed', () => {
    mainWindow = null;
  });

  Menu.setApplicationMenu(Menu.buildFromTemplate([
    {
      label: 'Application',
      submenu: [
        { label: 'Quit', accelerator: 'Command+Q', click: () => app.quit() }
      ]
    }, {
      label: 'Edit',
      submenu: [
        { label: 'Undo', accelerator: 'CmdOrCtrl+Z', selector: 'undo:' },
        { label: 'Redo', accelerator: 'Shift+CmdOrCtrl+Z', selector: 'redo:' },
        { type: 'separator' },
        { label: 'Cut', accelerator: 'CmdOrCtrl+X', selector: 'cut:' },
        { label: 'Copy', accelerator: 'CmdOrCtrl+C', selector: 'copy:' },
        { label: 'Paste', accelerator: 'CmdOrCtrl+V', selector: 'paste:' },
        { label: 'Select All', accelerator: 'CmdOrCtrl+A', selector: 'selectAll:' }
      ]
    }, {
      label: 'View',
      submenu: [
        {
          label: 'Reload',
          accelerator: 'CmdOrCtrl+R',
          click: (ignored, focusedWindow) => {
            if (focusedWindow) {
              focusedWindow.reload();
            }
          }
        }
      ]
    }
  ]));

  appIcon = new Tray(
    `${__dirname}/../resources/tray${process.platform === 'darwin' ? 'Template' : ''}.png`
  );
  appIcon.setToolTip('Felicemente');
  appIcon.setContextMenu(Menu.buildFromTemplate([
    {
      label: `Version: ${version}${process.env.NODE_ENV === 'development' ? '-dev' : ''}`,
      enabled: false,
    },
    {
      type: 'separator',
    },
    {
      label: 'Quit Felicemente',
      click: () => app.quit(),
    }
  ]));
});

ipcMain.on('signIn', (event, authUrl) => {
  authWindow = new BrowserWindow({
    title: 'Felicemente - Login',
    width: 800,
    height: 700,
    center: true,
    skipTaskbar: true,
    autoHideMenuBar: true,
    webPreferences: {
      nodeIntegration: false
    },
  });

  authWindow.loadURL(authUrl);

  authWindow.on('close', () => {
    event.sender.send('signedIn', 'closed');
    authWindow = null;
  });

  authWindow.webContents.on('will-navigate', (unused, url) => {
    handleOAuth2Callback(url);
  });

  authWindow.webContents.on('did-get-redirect-request', (unused, oldUrl, newUrl) => {
    handleOAuth2Callback(newUrl);
  });


  function handleOAuth2Callback(url) {
    const rawCode = /(?:code|access_token)=([^&]*)/.exec(url) || null;
    const code = (rawCode && rawCode.length > 1) ? rawCode[1] : null;
    const error = /\?error=(.+)$/.exec(url);

    if (code || error) {
      if (code) {
        event.sender.send('signedIn', null, code);
      } else if (error) {
        event.sender.send('signedIn', error);
      }
      setTimeout(() => {
        // Google login causes both will-redirect and did-get-redirect-request events to fire
        // hence we need to make sure we don't try to use authWindow when already destroyed
        if (authWindow) {
          authWindow.destroy();
          authWindow = null;
        }
      }, 10);
    }
  }
});
