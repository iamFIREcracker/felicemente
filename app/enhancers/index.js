import createConfigstorePersist from './configstore';
import createDrivePersist from './drive';

export default (app) => {
  app.factory('persistConfigstoreEnhancer', [
    'configStore',
    'googleService',
    'facebookService',
    '$injector',
    createConfigstorePersist
  ]);
  app.factory('persistDriveEnhancer', [
    'googleService',
    createDrivePersist
  ]);
};
