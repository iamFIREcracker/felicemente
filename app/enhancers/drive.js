import _ from 'underscore';

export default function createDrivePersist(googleService) {
  return next => (reducer, initialState, enhancer) => {
    if (typeof initialState === 'function' && typeof enhancer === 'undefined') {
      enhancer = initialState; // eslint-disable-line no-param-reassign
      initialState = undefined; // eslint-disable-line no-param-reassign
    }

    const store = next(reducer, initialState, enhancer);

    store.subscribe(() => {
      const state = store.getState();

      if (!_.isEmpty(state.google.token)) {
        googleService.saveThoughts(state.thoughts.thoughts)
          .doOnError((e) => {
            console.warn('Unable to persist state to Google:', e); // eslint-disable-line no-console
          })
          .subscribe();
      }
    });

    return store;
  };
}
