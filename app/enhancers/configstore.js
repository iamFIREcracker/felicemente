import _ from 'underscore';
import Rx from 'rx';

import { logOut } from '../actions/facebook';

export default function createConfigstorePersist(
  configStore,
  googleService,
  facebookService,
  $injector
) {
  return next => (reducer, initialState, enhancer) => {
    if (typeof initialState === 'function' && typeof enhancer === 'undefined') {
      enhancer = initialState; // eslint-disable-line no-param-reassign
      initialState = undefined; // eslint-disable-line no-param-reassign
    }

    let persistedState;
    let finalInitialState;

    try {
      persistedState = configStore.get('state');
      finalInitialState = _.assign({}, initialState, persistedState);
    } catch (e) {
      console.warn('Failed to retrieve initialize state from configstore:', e); // eslint-disable-line no-console
    }

    if (finalInitialState && finalInitialState.google) {
      finalInitialState.google.loading = false;
    }
    if (finalInitialState && finalInitialState.facebook) {
      finalInitialState.facebook.loading = false;
    }

    const store = next(reducer, finalInitialState, enhancer);

    store.subscribe(() => {
      const state = store.getState();

      try {
        configStore.set('state', state);
      } catch (e) {
        console.warn('Unable to persist state to configstore:', e); // eslint-disable-line no-console
      }
    });

    if (finalInitialState.google && !_.isEmpty(finalInitialState.google.token)) {
      if (!googleService.token) {
        googleService.setToken(finalInitialState.google.token);
      }
    }
    if (finalInitialState.facebook && finalInitialState.facebook.token) {
      if (!facebookService.token) {
        facebookService.setToken(finalInitialState.facebook.token)
          .doOnError(() => {
            $injector.get('$ngRedux').dispatch(logOut());
          })
          .onErrorResumeNext(Rx.Observable.empty())
          .subscribe();
      }
    }
    return store;
  };
}
