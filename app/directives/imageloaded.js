import angular from 'angular';

function ImageLoaded() {
  return {
    restrict: 'A',

    link: (scope, element, attrs) => {
      const cssClass = attrs.loadedclass;

      element.bind('load', () => {
        angular.element(element).addClass(cssClass);
      });
    }
  };
}

export default (app) => {
  app.directive('imageLoaded', [
    ImageLoaded
  ]);
};
