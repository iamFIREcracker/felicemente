import imagesLoaded from 'imagesloaded';
import MasonryLayout from 'masonry-layout';

function Masonry() {
  return {
    restrict: 'A',

    link: (scope, element, attrs) => {
      const mainClass = attrs.masonry;
      const options = attrs.masonryOptions;
      const reloadExpression = attrs.masonryReloadOn;

      const msnry = new MasonryLayout(
        mainClass,
        options
      );

      let firstRun = true;
      scope.$watch(reloadExpression, (newValue, oldValue) => {
        if (firstRun || newValue !== oldValue) {
          firstRun = false;
          msnry.reloadItems();
          imagesLoaded(mainClass, () => {
            msnry.layout();
          });
        }
      });
    }
  };
}

export default (app) => {
  app.directive('masonry', [
    Masonry
  ]);
};
