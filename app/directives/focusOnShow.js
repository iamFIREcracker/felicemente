function FocusOnShow($timeout) {
  return {
    restrict: 'A',
    link: ($scope, $element, $attr) => {
      function asyncFocus() {
        $timeout(() => {
          $element[0].focus();
          $element[0].select();
        }, 0);
      }

      if ($attr.ngShow) {
        $scope.$watch($attr.ngShow, (newValue) => {
          if (newValue) {
            asyncFocus();
          }
        });
      }
      if ($attr.ngHide) {
        $scope.$watch($attr.ngHide, (newValue) => {
          if (!newValue) {
            asyncFocus();
          }
        });
      }
    }
  };
}

export default (app) => {
  app.directive('focusOnShow', [
    '$timeout',
    FocusOnShow
  ]);
};
