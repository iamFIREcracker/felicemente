import focusOnShow from './focusOnShow';
import imageError from './imageError';
import imageLoaded from './imageLoaded';
import masonry from './masonry';
import onKeydownEnter from './onKeydownEnter';

export default (app) => {
  focusOnShow(app);
  imageError(app);
  imageLoaded(app);
  masonry(app);
  onKeydownEnter(app);
};
