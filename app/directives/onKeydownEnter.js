function OnKeydownEnter() {
  return {
    link: (scope, element, attrs) => {
      element.bind('keydown', (e) => {
        if (e.which === 13) {
          scope.$apply(() => {
            scope.$eval(attrs.onKeydownEnter, { e });
          });
          e.preventDefault();
        }
      });
    }
  };
}

export default (app) => {
  app.directive('onKeydownEnter', [
    OnKeydownEnter
  ]);
};
