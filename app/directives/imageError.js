function ImageError() {
  return {
    restrict: 'A',
    link: (scope, el, attr) => {
      el.on('error', () => {
        if (attr.imageError) {
          scope.$eval(attr.imageError);
        }
      });
    }
  };
}

export default (app) => {
  app.directive('imageError', [
    ImageError
  ]);
};

