import { ipcRenderer } from 'electron';
import Configstore from 'configstore';
import { name } from '../package.json';
import FacebookService from './facebook';
import GoogleService from './google';

function ConfigStore() {
  return new Configstore(name);
}

export default (app) => {
  app.service('configStore', ConfigStore);
  app.service('ipc', () => ipcRenderer);
  app.service('facebookService', FacebookService);
  app.service('googleService', GoogleService);
};
