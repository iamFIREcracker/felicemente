import FB from 'fb';
import Rx from 'rx';

FB.options({
  version: 'v2.8',
  appId: '354828151540938',
  appSecret: process.env.FB_APP_SECRET,
  redirectUri: 'https://www.facebook.com/connect/login_success.html',
  scope: [
    'email'
  ]
});

function apiResultOnly(res) {
  if (res.error) {
    throw res.error;
  }
  return res;
}

const api = Rx.Observable.fromCallback(FB.api, undefined, apiResultOnly);

class FacebookService {
  constructor(ipc) {
    this.ipc = ipc;
    this.api = api;
    this.token = null;
  }

  signIn() {
    const observable = new Rx.Subject();
    this.ipc.once('signedIn', (event, error, token) => {
      if (error) {
        observable.onError(error);
        return;
      }
      this.setToken(token)
        .flatMap(() => this.api('/me', { fields: ['email'] }))
        .doOnNext((response) => {
          observable.onNext({
            email: response.email,
            token
          });
          observable.onCompleted();
        })
        .doOnError((yetAnotherError) => {
          observable.onError(yetAnotherError);
        })
        .onErrorResumeNext(Rx.Observable.empty())
        .subscribe();
    });
    this.ipc.send('signIn', FB.getLoginUrl({
      responseType: 'token',
      display: 'popup'
    }));
    return observable;
  }

  setToken(token) {
    return this.getAppToken()
      .flatMap(appToken => this.api('/debug_token', {
        input_token: token,
        access_token: appToken,
      }))
      .doOnNext((response) => {
        if (!response.data.is_valid) {
          throw response.data.error;
        }
        FB.setAccessToken(token);
        this.token = token;
      })
      .map(Boolean);
  }

  getAppToken() {
    return this.api('oauth/access_token', {
      client_id: FB.options().appId,
      client_secret: FB.options().appSecret,
      grant_type: 'client_credentials'
    })
      .map(response => response.access_token);
  }

  signOut() { // eslint-disable-line class-methods-use-this
    return this.api('/me/permissions', 'delete')
      .catch((error) => {
        if (error.code === 190 // expired
            || error.code === 2500) { // invalid oauth token
          return Rx.Observable.just(true);
        }
        return Rx.Observable.throw(error);
      })
      .map(Boolean);
  }

  static $inject = [
    'ipc'
  ];
}

export default FacebookService;
