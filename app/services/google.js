import google from 'googleapis';
import Rx from 'rx';

export const OAUTH2 = {
  client_id: '342939124129-mtaead6dtrs168irf90oqccifqp23are.apps.googleusercontent.com',
  client_secret: process.env.GOOGLE_CLIENT_SECRET,
  redirect_url: 'http://localhost',
  access_type: 'offline',
  scope: [
    'email',
    'https://www.googleapis.com/auth/plus.me',
    'https://www.googleapis.com/auth/drive.appdata'
  ]
};

const OAuth2 = google.auth.OAuth2;
export const oauth2Client = new OAuth2(OAUTH2.client_id, OAUTH2.client_secret, OAUTH2.redirect_url);
google.options({ auth: oauth2Client });

function apiResultOnly(res) {
  return res;
}

const drive = google.drive('v3');
const driveFilesList = Rx.Observable.fromNodeCallback(drive.files.list, undefined, apiResultOnly);
const driveFilesCreate = Rx.Observable.fromNodeCallback(
  drive.files.create,
  undefined,
  apiResultOnly
);
const driveFilesGet = Rx.Observable.fromNodeCallback(drive.files.get, undefined, apiResultOnly);
const driveFilesUpdate = Rx.Observable.fromNodeCallback(
  drive.files.update,
  undefined,
  apiResultOnly
);
const plus = google.plus('v1');
const plusPeopleGet = Rx.Observable.fromNodeCallback(plus.people.get, undefined, apiResultOnly);

class GoogleService {
  constructor(ipc) {
    this.ipc = ipc;
    this.driveFilesList = driveFilesList;
    this.driveFilesCreate = driveFilesCreate;
    this.driveFilesGet = driveFilesGet;
    this.driveFilesUpdate = driveFilesUpdate;
    this.plusPeopleGet = plusPeopleGet;
    this.token = null;
  }

  signIn() {
    const observable = new Rx.Subject();
    this.ipc.once('signedIn', (event, error, code) => {
      if (error) {
        observable.onError(error);
        return;
      }
      oauth2Client.getToken(code, (anotherError, token) => {
        if (anotherError) {
          observable.onError(anotherError);
        }
        this.setToken(token);
        this.plusPeopleGet({
          userId: 'me'
        })
        .doOnNext((response) => {
          observable.onNext({
            email: response.emails[0].value,
            token
          });
          observable.onCompleted();
        })
        .doOnError((yetAnotherError) => {
          observable.onError(yetAnotherError);
        })
        .onErrorResumeNext(Rx.Observable.empty())
        .subscribe();
      });
    });
    this.ipc.send('signIn', oauth2Client.generateAuthUrl({
      access_type: OAUTH2.access_type,
      scope: OAUTH2.scope
    }));
    return observable;
  }

  setToken(token) {
    oauth2Client.setCredentials(token);
    this.token = token;
  }

  signOut() { // eslint-disable-line class-methods-use-this
    return Rx.Observable.just(true);
  }

  fetchThoughts() {
    return this.getConfigFileId()
    .flatMap(fileId => this.fetchConfig(fileId));
  }

  saveThoughts(thoughts) {
    return this.getConfigFileId()
    .flatMap(fileId => this.saveConfig(fileId, thoughts));
  }

  getConfigFileId() {
    const existingConfigFile = this.searchConfigFile();
    const configFileMissing = existingConfigFile
    .isEmpty()
    .filter(empty => empty);
    const newConfigFile = configFileMissing
    .flatMap(() => this.createConfigFile());

    return Rx.Observable.merge(
      existingConfigFile
      .pluck('id'),
      newConfigFile
    )
    .take(1);
  }

  searchConfigFile() {
    return this.driveFilesList({
      q: 'name = \'thoughts.json\'',
      spaces: 'appDataFolder',
      fields: 'nextPageToken, files(id, name)',
      maxResults: 1
    })
    .map(res => res.files[0]);
  }

  createConfigFile() {
    const resource = {
      name: 'thoughts.json',
      parents: [
        'appDataFolder'
      ]
    };
    const media = {
      mimeType: 'application/json',
      body: JSON.stringify([])
    };
    return this.driveFilesCreate({
      resource,
      media,
      fields: 'id'
    })
    .pluck('id');
  }

  fetchConfig(fileId) {
    return this.driveFilesGet({
      fileId,
      alt: 'media'
    });
  }

  saveConfig(fileId, thoughts) {
    const media = {
      mimeType: 'application/json',
      body: JSON.stringify(thoughts)
    };
    return this.driveFilesUpdate({ fileId, media });
  }

  static $inject = [
    'ipc'
  ];
}

export default GoogleService;
