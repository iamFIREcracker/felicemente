import thunk from 'redux-thunk';
import rootReducer from '../reducers';

const middlewares = [
  thunk,
  'lightboxMiddleware',
  'scrapeMiddleware',
  'facebookLoginService',
  'googleLoginMiddleware',
];

const enhancers = [
  'persistConfigstoreEnhancer',
];

export default function configureStore($ngReduxProvider, initialState) {
  $ngReduxProvider.createStoreWith(
    rootReducer,
    [...middlewares],
    [...enhancers],
    initialState
  );
}
