import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import rootReducer from '../reducers';

import * as mainActions from '../actions/index';
import * as googleActions from '../actions/google';
import * as facebookActions from '../actions/facebook';

const actionCreators = {
  ...mainActions,
  ...googleActions,
  ...facebookActions,
};

const logger = createLogger({
  level: 'info',
  collapsed: true
});

const middlewares = [
  thunk,
  'lightboxMiddleware',
  'scrapeMiddleware',
  'facebookLoginService',
  'googleLoginMiddleware',
  logger,
];

const enhancers = [
  'persistConfigstoreEnhancer',
  'persistDriveEnhancer',
  window.devToolsExtension ?
    window.devToolsExtension({ actionCreators }) :
      noop => noop,
];

export default function configureStore($ngReduxProvider, initialState) {
  $ngReduxProvider.createStoreWith(
    rootReducer,
    [...middlewares],
    [...enhancers],
    initialState
  );
}
