import {
  FB_CALL_LOGIN,
  FB_CALL_LOGOUT,
} from '../middleware/facebook';
import {
  FB_LOGIN_REQUEST,
  FB_LOGIN_SUCCESS,
  FB_LOGIN_FAILURE,
  FB_LOGOUT_REQUEST,
  FB_LOGOUT_SUCCESS,
  FB_LOGOUT_FAILURE,
} from '../constants';

export function logIn() {
  return {
    [FB_CALL_LOGIN]: {
      types: [
        FB_LOGIN_REQUEST,
        FB_LOGIN_SUCCESS,
        FB_LOGIN_FAILURE,
      ]
    }
  };
}

export function logOut() {
  return {
    [FB_CALL_LOGOUT]: {
      types: [
        FB_LOGOUT_REQUEST,
        FB_LOGOUT_SUCCESS,
        FB_LOGOUT_FAILURE,
      ]
    }
  };
}
