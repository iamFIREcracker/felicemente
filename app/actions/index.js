import { CALL_SCRAPE } from '../middleware/scrape';
import {
  RESET_ENTRY,
  ADD_HAPPY_THOUGHT,
  DELETE_HAPPY_THOUGHT,
  SHOW_HAPPY_THOUGHT,
  UPDATE_HAPPY_THOUGHT,
  EXTRACT_IMAGE_FOR_ENTRY_REQUEST,
  EXTRACT_IMAGE_FOR_ENTRY_SUCCESS,
  EXTRACT_IMAGE_FOR_ENTRY_FAILURE,
  EXTRACT_IMAGE_FOR_RELOAD_REQUEST,
  EXTRACT_IMAGE_FOR_RELOAD_SUCCESS,
  EXTRACT_IMAGE_FOR_RELOAD_FAILURE,
} from '../constants/';


export function extractImage(url) {
  return {
    [CALL_SCRAPE]: {
      types: [
        EXTRACT_IMAGE_FOR_ENTRY_REQUEST,
        EXTRACT_IMAGE_FOR_ENTRY_SUCCESS,
        EXTRACT_IMAGE_FOR_ENTRY_FAILURE
      ],
      url
    }
  };
}

export function addHappyThought(thought) {
  return (dispatch) => {
    dispatch({
      type: ADD_HAPPY_THOUGHT,
      payload: {
        thought
      }
    });
    dispatch({
      type: RESET_ENTRY
    });
  };
}

export function reloadHappyThoughtImage(id, url) {
  return {
    [CALL_SCRAPE]: {
      types: [
        EXTRACT_IMAGE_FOR_RELOAD_REQUEST,
        EXTRACT_IMAGE_FOR_RELOAD_SUCCESS,
        EXTRACT_IMAGE_FOR_RELOAD_FAILURE
      ],
      url
    },
    payload: {
      id
    }
  };
}

export function deleteHappyThought(id) {
  return {
    type: DELETE_HAPPY_THOUGHT,
    payload: {
      id
    }
  };
}

export function showHappyThought(id) {
  return {
    type: SHOW_HAPPY_THOUGHT,
    payload: {
      id
    }
  };
}

export function updateHappyThought(id, field, value) {
  return {
    type: UPDATE_HAPPY_THOUGHT,
    payload: {
      id,
      field,
      value
    }
  };
}
