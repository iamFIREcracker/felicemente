import {
  CALL_LOGIN,
  CALL_LOGOUT,
} from '../middleware/google';
import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  LOGOUT_FAILURE,
} from '../constants';

export function logIn() {
  return {
    [CALL_LOGIN]: {
      types: [
        LOGIN_REQUEST,
        LOGIN_SUCCESS,
        LOGIN_FAILURE,
      ]
    }
  };
}

export function logOut() {
  return {
    [CALL_LOGOUT]: {
      types: [
        LOGOUT_REQUEST,
        LOGOUT_SUCCESS,
        LOGOUT_FAILURE,
      ]
    }
  };
}
