import createLightboxMiddleware from './lightbox';
import createScrapeMiddleware from './scrape';
import createGoogleLoginMiddleware from './google';
import createFacebookLoginMiddleware from './facebook';

export default (app) => {
  app.factory('lightboxMiddleware', [
    'Lightbox',
    createLightboxMiddleware
  ]);
  app.factory('scrapeMiddleware', [
    '$http',
    'facebookService',
    createScrapeMiddleware
  ]);
  app.factory('googleLoginMiddleware', [
    'googleService',
    createGoogleLoginMiddleware
  ]);
  app.factory('facebookLoginService', [
    'facebookService',
    createFacebookLoginMiddleware
  ]);
};
