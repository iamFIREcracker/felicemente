import Rx from 'rx';
import {
  MERGE_HAPPY_THOUGHTS
} from '../constants';

export const CALL_LOGIN = 'CALL_LOGIN';
export const CALL_LOGOUT = 'CALL_LOGOUT';

export default function createGoogleLoginMiddleware(googleService) {
  return () => next => (action) => {
    const callLogin = action[CALL_LOGIN];
    const callLogout = action[CALL_LOGOUT];
    const call = callLogin || callLogout;
    if (typeof call === 'undefined') {
      return next(action);
    }

    const { types } = call;

    function actionWith(data) {
      const finalAction = {
        ...action,
        ...data
      };
      if (callLogout) {
        delete finalAction[CALL_LOGOUT];
      } else {
        delete finalAction[CALL_LOGIN];
      }
      return finalAction;
    }

    const [
      requestType,
      successType,
      failureType
    ] = types;
    next(actionWith({
      type: requestType
    }));

    let observable;
    if (callLogout) {
      observable = googleService.signOut()
      .doOnNext(() => {
        next(actionWith({
          type: successType
        }));
      });
    } else {
      observable = googleService.signIn()
      .doOnNext((response) => {
        next(actionWith({
          type: successType,
          payload: {
            email: response.email,
            token: response.token
          }
        }));
      })
      .flatMap(
        () => googleService
        .fetchThoughts()
        .onErrorResumeNext(Rx.Observable.empty())
      )
      .doOnNext((thoughts) => {
        next({
          type: MERGE_HAPPY_THOUGHTS,
          payload: {
            thoughts
          }
        });
      });
    }

    observable.doOnError((error) => {
      next(actionWith({
        type: failureType,
        error
      }));
    })
    .onErrorResumeNext(Rx.Observable.empty())
    .subscribe();
  };
}
