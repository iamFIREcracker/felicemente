import Rx from 'rx';

export const FB_CALL_LOGIN = 'FB_CALL_LOGIN';
export const FB_CALL_LOGOUT = 'FB_CALL_LOGOUT';

export default function createFacebookLoginMiddleware(facebookService) {
  return () => next => (action) => {
    const callLogin = action[FB_CALL_LOGIN];
    const callLogout = action[FB_CALL_LOGOUT];
    const call = callLogin || callLogout;
    if (typeof call === 'undefined') {
      return next(action);
    }

    const { types } = call;

    function actionWith(data) {
      const finalAction = {
        ...action,
        ...data
      };
      if (callLogout) {
        delete finalAction[FB_CALL_LOGOUT];
      } else {
        delete finalAction[FB_CALL_LOGIN];
      }
      return finalAction;
    }

    const [
      requestType,
      successType,
      failureType
    ] = types;
    next(actionWith({
      type: requestType
    }));

    let observable;
    if (callLogout) {
      observable = facebookService.signOut()
      .doOnNext(() => {
        next(actionWith({
          type: successType
        }));
      });
    } else {
      observable = facebookService.signIn()
      .doOnNext((response) => {
        next(actionWith({
          type: successType,
          payload: {
            email: response.email,
            token: response.token
          }
        }));
      });
    }

    observable.doOnError((error) => {
      next(actionWith({
        type: failureType,
        error
      }));
    })
    .onErrorResumeNext(Rx.Observable.empty())
    .subscribe();
  };
}
