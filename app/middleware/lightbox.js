import { SHOW_HAPPY_THOUGHT } from '../constants';
import {
  getThoughts,
  getExampleThoughts
} from '../reducers/thoughts';

export default function createLightboxMiddleware(Lightbox) {
  return store => next => (action) => {
    if (action.type !== SHOW_HAPPY_THOUGHT) {
      return next(action);
    }

    next(action);
    const { payload: { id } } = action;
    const state = store.getState();
    let thoughts = getThoughts(state);
    if (thoughts.length === 0) {
      thoughts = getExampleThoughts(state);
    }
    Lightbox.openModal(thoughts, id);
  };
}
