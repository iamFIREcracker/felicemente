import Rx from 'rx';
import cheerio from 'cheerio';

export const CALL_SCRAPE = 'CALL_SCRAPE';

function extractUrlOfBiggestImage(source) {
  return source.filter(img => img.height * img.width > 0).reduce((acc, current) => {
    let nextAcc = acc;
    if (acc === null || current.width * current.height > acc.width * acc.height) {
      nextAcc = current;
    }
    return nextAcc;
  }, null)
  .pluck('url');
}

function extractWithEmbedly($http, url) {
  const req = $http({
    method: 'GET',
    url: 'http://api.embed.ly/1/extract',
    params: {
      key: process.env.EMBEDLY_KEY,
      format: 'json',
      url
    }
  });
  return Rx.Observable.fromPromise(req)
  .flatMap(response => (
    Rx.Observable.fromArray(response.data.images)
  ))
  .let(o => extractUrlOfBiggestImage(o));
}

function scrapeGoogle(data) {
  const scraped = [];
  const rawUrl = /data-url="([^"]*)"/.exec(data) || null;
  const url = (rawUrl && rawUrl.length > 1) ? rawUrl[1] : null;

  if (url) {
    scraped.push({
      url,
      width: /data-width="([^"]*)"/.exec(data)[1],
      height: /data-height="([^"]*)"/.exec(data)[1],
    });
  }
  return scraped;
}

function scrapeFacebook(data) {
  const $ = cheerio.load(data);
  return $('a img').map((i, elem) => {
    const img = $(elem);
    return {
      url: img.attr('src'),
      width: parseInt(img.attr('width') || 0, 10),
      height: parseInt(img.attr('height') || 0, 10),
    };
  }).get();
}

function scrape($http, url) {
  const req = $http({
    method: 'GET',
    url,
  });
  return Rx.Observable.fromPromise(req)
  .map((response) => {
    let data = response.data;
    data = data.replace(/<!--/g, '');
    data = data.replace(/-->/g, '');
    return Array.prototype.concat(
      scrapeGoogle(data),
      scrapeFacebook(data)
    );
  })
  .flatMap(images => Rx.Observable.fromArray(images))
  .let(o => extractUrlOfBiggestImage(o));
}

export default function createScrapeMiddleware($http, facebookService) {
  return () => next => (action) => {
    const callScrape = action[CALL_SCRAPE];
    if (typeof callScrape === 'undefined') {
      return next(action);
    }

    const { url, types } = callScrape;

    function actionWith(data) {
      const finalAction = {
        ...action,
        ...data
      };
      delete finalAction[CALL_SCRAPE];
      return finalAction;
    }

    const [requestType, successType, failureType] = types;
    next(actionWith({
      type: requestType,
      url
    }));

    if (!url) {
      return next(actionWith({
        type: failureType,
        error: 'empty'
      }));
    }

    if (url.match(/(fbcdn.net|googleusercontent.com)/)) {
      return next(actionWith({
        type: failureType,
        error: 'cdn'
      }));
    }

    scrape($http, url)
    .catch(() => extractWithEmbedly($http, url))
    .doOnError(() => {
      next(actionWith({
        type: failureType,
        error: 'image'
      }));
    })
    .doOnNext((imgUrl) => {
      next(actionWith({
        type: successType,
        response: {
          url: imgUrl
        }
      }));
    })
    .catch(Rx.Observable.just(''))
    .subscribe();
  };
}
