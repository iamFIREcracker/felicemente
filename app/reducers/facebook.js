import { handleActions } from 'redux-actions';
import {
  FB_LOGIN_REQUEST,
  FB_LOGIN_SUCCESS,
  FB_LOGIN_FAILURE,
  FB_LOGOUT_SUCCESS
} from '../constants';

const initialState = {
  loading: false,
  email: '',
  token: null
};

export default handleActions({
  [FB_LOGIN_REQUEST]: state => ({
    ...state,
    loading: true
  }),
  [FB_LOGIN_SUCCESS]: (state, action) => {
    const { payload: { email, token } } = action;
    return {
      ...state,
      loading: false,
      email,
      token
    };
  },
  [FB_LOGIN_FAILURE]: state => ({
    ...state,
    loading: false
  }),
  [FB_LOGOUT_SUCCESS]: state => ({
    ...state,
    email: '',
    token: null
  })
}, initialState);

export function getEmail(state) {
  return state.facebook.email;
}

export function getLoadingStatus(state) {
  return state.facebook.loading;
}
