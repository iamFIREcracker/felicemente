import _ from 'underscore';
import { handleActions } from 'redux-actions';
import {
  RESET_ENTRY,
  ADD_HAPPY_THOUGHT,
  DELETE_HAPPY_THOUGHT,
  MERGE_HAPPY_THOUGHTS,
  UPDATE_HAPPY_THOUGHT,
  EXTRACT_IMAGE_FOR_ENTRY_REQUEST,
  EXTRACT_IMAGE_FOR_ENTRY_SUCCESS,
  EXTRACT_IMAGE_FOR_ENTRY_FAILURE,
  EXTRACT_IMAGE_FOR_RELOAD_SUCCESS,
  EXTRACT_IMAGE_FOR_RELOAD_FAILURE,
} from '../constants';

const initialState = {
  entry: {
    url: '',
    src: '',
    error: '',
    loading: false
  },
  thoughts: [],
  exampleThoughts: [
    {
      name: 'Awesome photo',
      originalUrl: 'http://lorempixel.com/400/250/abstract',
      src: 'http://lorempixel.com/400/250/abstract',
    },
    {
      name: 'Great photo',
      originalUrl: 'http://lorempixel.com/450/450/city',
      src: 'http://lorempixel.com/450/450/city',
    },
    {
      name: 'Strange photo',
      originalUrl: 'http://lorempixel.com/400/300/people',
      src: 'http://lorempixel.com/400/300/people',
    },
    {
      name: 'Brilliant photo',
      originalUrl: 'http://lorempixel.com/350/200/space',
      src: 'http://lorempixel.com/350/200/space',
    }
  ]
};

const sortedByDate = thoughts => _.sortBy(thoughts, t => new Date(t.date)).reverse();

export default handleActions({
  [RESET_ENTRY]: (state) => {
    const { entry } = state;
    return {
      ...state,
      entry: {
        ...entry,
        src: '',
        url: '',
        error: ''
      }
    };
  },
  [ADD_HAPPY_THOUGHT]: (state, action) => {
    const { thoughts } = state;
    const { payload: { thought } } = action;
    return {
      ...state,
      thoughts: sortedByDate([
        ...thoughts,
        thought
      ])
    };
  },
  [DELETE_HAPPY_THOUGHT]: (state, action) => {
    const { thoughts } = state;
    const { payload: { id } } = action;
    return {
      ...state,
      thoughts: [
        ...thoughts.slice(0, id),
        ...thoughts.slice(id + 1)
      ]
    };
  },
  [MERGE_HAPPY_THOUGHTS]: (state, action) => {
    const { payload: { thoughts: remote } } = action;
    const thoughts = [...state.thoughts];
    _.forEach(remote, (remoteThought) => {
      const index = _.findIndex(thoughts, thought => (
        remoteThought.src === thought.src
      ));
      if (index === -1) {
        thoughts.push(remoteThought);
      }
    });
    return {
      ...state,
      thoughts: sortedByDate(thoughts)
    };
  },
  [UPDATE_HAPPY_THOUGHT]: (state, action) => {
    const { thoughts } = state;
    const { payload: { id, field, value } } = action;
    return {
      ...state,
      thoughts: sortedByDate([
        ...thoughts.slice(0, id),
        {
          ...thoughts[id],
          [field]: value
        },
        ...thoughts.slice(id + 1)
      ])
    };
  },
  [EXTRACT_IMAGE_FOR_ENTRY_REQUEST]: (state, action) => {
    const { entry } = state;
    const { url } = action;
    return {
      ...state,
      entry: {
        ...entry,
        url,
        src: '',
        loading: true
      }
    };
  },
  [EXTRACT_IMAGE_FOR_ENTRY_SUCCESS]: (state, action) => {
    const { entry, thoughts } = state;
    const { response: { url } } = action;
    const duplicate = _.find(thoughts, t => t.src === url);
    return {
      ...state,
      entry: {
        ...entry,
        src: duplicate ? '' : url,
        error: duplicate ? 'unique' : '',
        loading: false
      }
    };
  },
  [EXTRACT_IMAGE_FOR_ENTRY_FAILURE]: (state, action) => {
    const { entry } = state;
    const { url } = entry;
    const { error } = action;
    return {
      ...state,
      entry: {
        ...entry,
        error: url ? error : '',
        loading: false
      }
    };
  },
  [EXTRACT_IMAGE_FOR_RELOAD_SUCCESS]: (state, action) => {
    const { thoughts } = state;
    const { response: { url } } = action;
    const { payload: { id } } = action;
    return {
      ...state,
      thoughts: [
        ...thoughts.slice(0, id),
        {
          ...thoughts[id],
          src: url,
          error: ''
        },
        ...thoughts.slice(id + 1)
      ]
    };
  },
  [EXTRACT_IMAGE_FOR_RELOAD_FAILURE]: (state, action) => {
    const { thoughts } = state;
    const { error } = action;
    const { payload: { id } } = action;
    return {
      ...state,
      thoughts: [
        ...thoughts.slice(0, id),
        {
          ...thoughts[id],
          error
        },
        ...thoughts.slice(id + 1)
      ]
    };
  },
}, initialState);

export function getEnteredUrl(state) {
  return state.thoughts.entry.url;
}

export function getResolvedUrl(state) {
  return state.thoughts.entry.src;
}

export function getEntryErrorStatus(state) {
  return state.thoughts.entry.error;
}

export function getEntryLoadingStatus(state) {
  return state.thoughts.entry.loading;
}

export function getThoughts(state) {
  return state.thoughts.thoughts;
}

export function getExampleThoughts(state) {
  return state.thoughts.exampleThoughts;
}
