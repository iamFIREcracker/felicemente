import { handleActions } from 'redux-actions';
import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_SUCCESS
} from '../constants';

const initialState = {
  loading: false,
  email: '',
  token: {}
};

export default handleActions({
  [LOGIN_REQUEST]: state => ({
    ...state,
    loading: true
  }),
  [LOGIN_SUCCESS]: (state, action) => {
    const { payload: { email, token } } = action;
    return {
      ...state,
      loading: false,
      email,
      token
    };
  },
  [LOGIN_FAILURE]: state => ({
    ...state,
    loading: false
  }),
  [LOGOUT_SUCCESS]: state => ({
    ...state,
    email: '',
    token: {}
  })
}, initialState);

export function getEmail(state) {
  return state.google.email;
}

export function getLoadingStatus(state) {
  return state.google.loading;
}
