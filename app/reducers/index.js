import { combineReducers } from 'redux';
import google from './google';
import facebook from './facebook';
import thoughts from './thoughts';

const rootReducer = combineReducers({
  google,
  facebook,
  thoughts,
});

export default rootReducer;
